let generateMenu = ""

const btnEng = document.getElementById('btn-eng')
const btnIta = document.getElementById('btn-ita')

function openItalianMenu(){
  generateMenu = `
    <embed 
      class="viewer"
      src="/bellaNapoliMenu/ITA_BellaNapoli_Rapallo_24_9.pdf" 
      type="application/pdf" 
      frameBorder="0" 
      scrolling="auto">
    </embed>
  `
  document.getElementById('menu-holder').innerHTML = generateMenu
  btnIta.style.visibility ="hidden"
  btnEng.style.visibility = "visible"
  console.log('generateMenu')
}
 
function openEnglishMenu(){
  generateMenu = `
    <embed 
      class="viewer"
      src="/bellaNapoliMenu/ENG_BellaNapoli_Rapallo_24_9.pdf" 
      type="application/pdf" 
      frameBorder="0" 
      scrolling="auto">
    </embed>
  `
  document.getElementById('menu-holder').innerHTML = generateMenu
  btnEng.style.visibility = "hidden"
  btnIta.style.visibility = "visible"

}
openItalianMenu()


btnEng.addEventListener('click', openEnglishMenu)
btnIta.addEventListener('click', openItalianMenu)
